var openButton = document.getElementsByClassName("accordion__button");
var accContent = document.getElementsByClassName("accordion__content");

for (let i = 0; i < openButton.length; i++) {
  openButton[i].addEventListener("click", () => {
    //remove all active tab
    if (!accContent[i].classList.contains("active")) {
      hideContent();
    }

    //Active tab
    accContent[i].classList.toggle("active");
    if (accContent[i].classList.contains("active")) {
      openButton[i].style.transform = "rotate(45deg)";
    } else {
      openButton[i].style.transform = "rotate(0)";
    }
  });
}

const hideContent = () => {
  const accContent = document.getElementsByClassName("accordion__content");
  for (let i = 0; i < accContent.length; i++) {
    accContent[i].classList.remove("active");
    openButton[i].style.transform = "rotate(0)";
  }
};
